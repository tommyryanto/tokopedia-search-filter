//
//  ProductCollectionViewCell.swift
//  Tokopedia Search Filter
//
//  Created by Tommy Ryanto on 23/10/20.
//  Copyright © 2020 Tommy Ryanto. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    
}
