//
//  ShopTypeTableViewCell.swift
//  Tokopedia Search Filter
//
//  Created by Tommy Ryanto on 25/10/20.
//  Copyright © 2020 Tommy Ryanto. All rights reserved.
//

import UIKit

class ShopTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var checkedImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
