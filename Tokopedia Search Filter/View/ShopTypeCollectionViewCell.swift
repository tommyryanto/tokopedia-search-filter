//
//  ShopTypeCollectionViewCell.swift
//  Tokopedia Search Filter
//
//  Created by Tommy Ryanto on 25/10/20.
//  Copyright © 2020 Tommy Ryanto. All rights reserved.
//

import UIKit

class ShopTypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var removeBtn: UIButton!
}
