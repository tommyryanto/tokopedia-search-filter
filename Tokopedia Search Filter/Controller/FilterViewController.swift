//
//  FilterViewController.swift
//  Tokopedia Search Filter
//
//  Created by Tommy Ryanto on 24/10/20.
//  Copyright © 2020 Tommy Ryanto. All rights reserved.
//

import UIKit
import TTRangeSlider

class FilterViewController: UIViewController {
    
    var shopTypes: [String] = []
    
    @IBOutlet weak var wholeSaleSwitch: UISwitch!
    @IBAction func applyBtn(_ sender: UIButton) {
        performSegue(withIdentifier: "back", sender: self)
    }
    @IBOutlet weak var maximumPriceTF: UITextField!
    @IBOutlet weak var minimumPriceTF: UITextField!
    @IBOutlet weak var priceRange: TTRangeSlider!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBAction func moveToShopTypesBtn(_ sender: Any) {
        performSegue(withIdentifier: "moveToShopTypes", sender: self)
    }
    
    @IBAction func backToHomeFilter(_ sender: UIStoryboardSegue) {
        collectionView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "back" {
            let dest = segue.destination as! ViewController
            let pmin = Int(priceRange.selectedMinimum)
            let pmax = Int(priceRange.selectedMaximum)
            let wholeSale = wholeSaleSwitch.isOn
            let official = shopTypes.contains("Official Store")
            let goldSeller = shopTypes.contains("Gold Merchant") ? 2 : 0
            let filterQuery = "&pmin=\(pmin)&pmax=\(pmax)&wholesale=\(wholeSale)&official=\(official)&fshop=\(goldSeller)&start=0&rows=10"
            dest.filterQuery = filterQuery
        }
    }

}

extension FilterViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shopTypes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "typeCell", for: indexPath) as! ShopTypeCollectionViewCell
        
        cell.removeBtn.tag = indexPath.row
        cell.typeLabel.text = shopTypes[indexPath.row]
        cell.removeBtn.addTarget(self, action: #selector(removeShopTypes(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width / 2.0 - 10
        let height = CGFloat(41)

        return CGSize(width: width, height: height)
    }
    
    @objc func removeShopTypes(sender: UIButton) {
        shopTypes.remove(at: sender.tag)
        collectionView.reloadData()
    }
    
}

extension FilterViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == minimumPriceTF {
            priceRange.selectedMinimum = Float(minimumPriceTF.text!)!
        } else if textField == maximumPriceTF {
            priceRange.selectedMaximum = Float(maximumPriceTF.text!)!
        }
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == minimumPriceTF {
            priceRange.selectedMinimum = Float(minimumPriceTF.text!)!
        } else if textField == maximumPriceTF {
            priceRange.selectedMaximum = Float(maximumPriceTF.text!)!
        }
        return true
    }
    
}

extension FilterViewController: TTRangeSliderDelegate {
    func rangeSlider(_ sender: TTRangeSlider!, didChangeSelectedMinimumValue selectedMinimum: Float, andMaximumValue selectedMaximum: Float) {
        minimumPriceTF.text = "\(selectedMinimum)"
        maximumPriceTF.text = "\(selectedMaximum)"
    }
}
