//
//  ShopTypeViewController.swift
//  Tokopedia Search Filter
//
//  Created by Tommy Ryanto on 25/10/20.
//  Copyright © 2020 Tommy Ryanto. All rights reserved.
//

import UIKit

class ShopTypeViewController: UIViewController {
    
    @IBAction func applyBtn(_ sender: UIButton) {
        performSegue(withIdentifier: "backToFilter", sender: self)
    }
    
    var type = ["Gold Merchant", "Official Store"]
    var checked  = [false, false]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "backToFilter" {
            var chosenTypes: [String] = []
            for i in 0..<type.count {
                if checked[i] {
                    chosenTypes.append(type[i])
                }
            }
            let mainFilterVC = segue.destination as! FilterViewController
            mainFilterVC.shopTypes = chosenTypes
        }
    }

}

extension ShopTypeViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return type.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "typeCell") as! ShopTypeTableViewCell
        
        cell.typeLabel.text = type[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ShopTypeTableViewCell
        if !checked[indexPath.row] {
            checked[indexPath.row] =  true
            cell.checkedImg.image = UIImage(named: "checked")
        } else {
            checked[indexPath.row] = false
            cell.checkedImg.image = UIImage(named: "not-checked")
        }
    }
}
