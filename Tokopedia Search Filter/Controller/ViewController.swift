//
//  ViewController.swift
//  Tokopedia Search Filter
//
//  Created by Tommy Ryanto on 22/10/20.
//  Copyright © 2020 Tommy Ryanto. All rights reserved.
//

import UIKit
import SystemConfiguration

class ViewController: UIViewController {
    
    var filterQuery = ""
    
    @IBAction func backToProduct(_ sender: UIStoryboardSegue) {
        retrieveData()
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    let BASE_URL = "https://ace.tokopedia.com/search/v2.5/product?q=samsung"
    var results = [Products]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        checkConnection()
    }
    
    func checkConnection() {
        if isConnectedToNetwork() {
            retrieveData()
        } else {
            let alert = UIAlertController(title: "Error", message: "No Internet Connection", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Try Again", style: .default, handler: { (action) in
                self.checkConnection()
            }))
            self.present(alert, animated: true) {
                
            }
        }
    }
    
    func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)

        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }

        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)

        return ret
    }
    
    func retrieveData() {
        print("filterQuery = \(filterQuery)")
        print("\(BASE_URL)\(filterQuery)")
        results = []
        if let url = URL(string: "\(BASE_URL)\(filterQuery)") {
            let session = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error == nil {
                    do {
                        let resp = try JSONDecoder().decode(Results.self, from: data!)
                        self.results = resp.data
                        
                        DispatchQueue.main.async {
                        //var counter = 0
                        
                        /*for res in self.results {
                            
                            if counter < self.localDataCount {
                                self.db.insert(id: counter, original_title: res.original_title, release_date: res.release_date)
                            }
                            counter += 1
                        }*/
                            self.collectionView.reloadData()
                        }
                    } catch {
                        print("error")
                    }
                } else {
                    print(error!.localizedDescription)
                }
            }
            session.resume()
        }
    }

}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.results.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width / 2.0 - 10
        let height = CGFloat(237)

        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productCell", for: indexPath) as! ProductCollectionViewCell
        
        let data = try? Data(contentsOf: URL(string: self.results[indexPath.row].image_uri)!)
        cell.productImage.image = UIImage(data: data!)
        cell.productName.text = self.results[indexPath.row].name
        cell.productPrice.text = "\(self.results[indexPath.row].price)"
        cell.layer.borderWidth = 1
        
        return cell
    }
}
