//
//  Products.swift
//  Tokopedia Search Filter
//
//  Created by Tommy Ryanto on 26/10/20.
//  Copyright © 2020 Tommy Ryanto. All rights reserved.
//

import Foundation

struct Results: Codable {
    var data: [Products]
}

struct Products: Codable {
    var name: String
    var image_uri: String
    var price: String
    private enum DataCodingKeys: String, CodingKey {
        case name
        case image_uri
        case price
    }
}
